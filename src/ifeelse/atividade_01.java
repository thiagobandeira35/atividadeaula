package ifeelse;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class atividade_01 {

	public static void main(String[] args) {

		String pergunta1 = "Por for favor informar o primeiro valor ";
		String pergunta2 = "Por for favor informar o segundo valor ";
		String telaFinal = "Resultado: ";

		boolean primeiraChave = true;
		boolean segundaChave = true;

		String valorEmTexto1 = null;
		String valorEmTexto2 = null;

		JFrame frame = new JFrame("exemplo");

		while (primeiraChave) {

			valorEmTexto1 = JOptionPane.showInputDialog(frame, pergunta1, JOptionPane.INFORMATION_MESSAGE);

			if (valorEmTexto1 == null || valorEmTexto1.isEmpty()) {

				JOptionPane.showMessageDialog(frame, "O valor não foi informado, tente novamente !");

			} else {

				primeiraChave = false;

			}
		}

		while (segundaChave) {

			valorEmTexto2 = JOptionPane.showInputDialog(frame, pergunta2, JOptionPane.INFORMATION_MESSAGE);

			if (valorEmTexto2 == null || valorEmTexto2.isEmpty()) {

				JOptionPane.showMessageDialog(frame, "O valor não foi informado, tente novamente !");

			} else {

				segundaChave = false;

			}

			JOptionPane.showMessageDialog(frame, "Calculando.....");

			Integer primeiroValor = null;
			Integer segundoValor = null;

			if (valorEmTexto1 != null && !valorEmTexto1.isEmpty()) {

				primeiroValor = Integer.parseInt(valorEmTexto1);

			}
			;

			if (valorEmTexto2 != null && !valorEmTexto2.isEmpty()) {

				segundoValor = Integer.parseInt(valorEmTexto2);

			}
			;

			if (valorEmTexto1.equals(valorEmTexto2)) {

				JOptionPane.showMessageDialog(frame, "Os valores estão iguais, favor informar novamente");

			} else if (primeiroValor > segundoValor) {

				System.out.println("O primeiro valor foi maior => " + telaFinal + valorEmTexto1);

			} else if (segundoValor > primeiroValor) {

				System.out.println("O segundo valor foi maior => " + telaFinal + valorEmTexto2);

			}

		}
	}

}
